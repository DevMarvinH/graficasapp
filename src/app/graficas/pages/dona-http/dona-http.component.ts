import { Component, OnInit } from '@angular/core';
import { ChartData, ChartType } from 'chart.js';
import { GraficasService } from '../../services/graficas.service';

@Component({
  selector: 'app-dona-http',
  templateUrl: './dona-http.component.html',
  styles: [
  ]
})
export class DonaHttpComponent implements OnInit {

  public doughnutChartLabels: string[] = [ 
    //'Download Sales', 'In-Store Sales', 'Mail-Order Sales' 
  ];
  public doughnutChartData: ChartData<'doughnut'> = {
    labels: this.doughnutChartLabels,
    datasets: [
      { 
        data: [],
        // data: [ 350, 450, 100 ], 
        backgroundColor: [ '#EB0055', '#F500AE', '#8C09E8' ] 
      },
    ]
  };
  public doughnutChartType: ChartType = 'doughnut';

  constructor(private graficasService: GraficasService) {}
  
  ngOnInit(): void {
    // this.graficasService.getUsuariosRedesSociales()
    //   .subscribe(data => {
    //     //Labels
    //     const labels: string[] = Object.keys(data);
    //     this.doughnutChartData.labels = labels;
    //     console.log(labels);
        
    //     //Data
    //     const values: number[] = Object.values(data);
    //     this.doughnutChartData.datasets[0].data = values;
    //   });

    this.graficasService.getUsuariosRedesSocialesDonaData()
      .subscribe(({ labels, values }) => {
        this.doughnutChartData.labels = labels;
        this.doughnutChartData.datasets[0].data = values;
      });
  }

}
